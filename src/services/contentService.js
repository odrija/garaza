import React from "react";

// prettier-ignore
const content = [
  {
    _id: "star",
    intro: {
      introImage: "/images/star/intro-image.jpg",
      introLogoImage: "/images/star/intro-logo.png",
      title: "Star 3 ½ HP",
      subTitle:
        "Pirmais sērijveidā ražotais automobilis Lielbritānijā, izgatavots 200 eksemplāros",
      text: (
        <React.Fragment>
          <p>
            Uzņēmums <a href="https://en.wikipedia.org/wiki/Star_Motor_Company" target="_blank">Star Motor Company</a> šo modeli sāk ražot
            1899. gadā pēc <a href="https://de.wikipedia.org/wiki/Benz_%26_Cie." target="_blank">Benz & Cie</a> licences.
          </p>
          <p>
            Tehniski un vizuāli Star ir tuvs <a href="https://en.wikipedia.org/wiki/Benz_Velo" target="_blank">Benz Velo</a>, kas
            uzskatāms par pirmo sērijveidā ražoto automobili pasaules autobūves
            vēsturē.
          </p>
          <p>
            Automobiļa motors novietots aizmugurē, un to iedarbina, ar roku
            iegriežot spararatu.
          </p>
          <p>
            Uzsākot ražošanu, Star darbnīcā nedēļā izgatavo vienu automobili,
            bet 1900. gadā – jau 20 automobiļus nedēļā.
          </p>
          <p>
            Aplūkojamais auto, kas saglabāts oriģinālā stāvoklī, regulāri
            piedalījies leģendārajā <a href="https://www.veterancarrun.com/" target="_blank">Londona-Braitona</a> braucienā.
            2019. gadā šo automobili izsolē iegādājas kolekcionārs no Latvijas.
          </p>
        </React.Fragment>
      ),
      sideImage: "/images/star/side-image.jpg",
    },
    logoImage: "/images/star/logo.png",
    imageList: [
      {
        _id: 1,
        image: "/images/star/image-1.jpg",
        caption:
          "1898. gada Star 3½ hp, pirmais automobīlis Oklendā, Jaunzēlandē.",
        source: "Avots: Wikipedia.",
        sourceUrl: "https://en.wikipedia.org/wiki/Star_Motor_Company",
      },
      {
        _id: 2,
        image: "/images/star/image-2.jpg",
        caption:
          "Star Cycle Company Ltd. rūpnīcas fasāde Pontnija vai Stjuarta ielā Volverhemptonā.",
        source:
          "Avots: Wolverhampton Arts and Museums Service, blackcountryhistory.org.",
        sourceUrl: "http://blackcountryhistory.org/",
      },
    ],
    imageGallery: {
      caption: "Foto no izstādes “Garāža” atklāšanas 2020. gada 20. februārī.",
      images: [
        {
          _id: 1,
          src: "/images/star/gallery-1.jpg"
        }
      ]
    },
    technicalData: [
      {
        key: "Valsts",
        value: "Lielbritānija"
      },
      { 
        key: "Ražošanas gads",
        value: 1899,
      },
      { 
        key: "Motora tilpums",
        value: 1296,
      },
      { 
        key: "Motora jauda",
        value: 3.5,
      },
      
      { 
        key: "Cilindru skaits",
        value: 1,
      },
      { 
        key: "Maksimālais ātrums",
        value: 14.5,
      },
      { 
        key: "Pašmasa",
        value: null,
      },
    ]
  },
  {
    _id: "stanley",
    intro: {
      introImage: "/images/stanley/intro-image.jpg",
      introLogoImage: "/images/stanley/intro-logo.png",
      title: "Stanley model 63 Toy Tonneau",
      subTitle:
        "Stanley tvaika auto pirmais autobūves vēsturē pārsniedz 200 km/h ātrumu",
      text: (
        <React.Fragment>
	      <p>Automobilis ražots brāļu Stenliju dibinātajā uzņēmumā <a href="https://en.wikipedia.org/wiki/Stanley_Motor_Carriage_Company" target="_blank">Stanley Motor Carriage Company</a> ASV. To darbina ūdens tvaiks, kura iegūšanai kā degvielu izmanto petroleju. Pie maksimālās jaudas līdz 420°C sakarsēta tvaika spiediens sasniedz 42 kg/cm<sup>2</sup>.</p>
        </React.Fragment>
      ),
      text2: (
	<React.Fragment>
	  <p>Autobūves pirmsākumos tvaika automobilis ir nopietns konkurents automobiļiem ar iekšdedzes motoru. Tieši <em>Stanley</em> tvaika auto ir pirmais autobūves vēsturē, kas jau 1906. gadā pārsniedz 200 km/h ātrumu.</p>
	  <p>Līdz 1924. gadam <em>Stanley Motor Carriage Company</em> saražo 10 tūkstošus tvaika automobiļu. To vadīšanai jāizmanto 13 dažādas vadības ierīces – ierastās papildina vairāki krāni un ventiļi, arī degvielas un ūdens sūkņu sviras.</p>
	</React.Fragment>
      ),
      sideImage: "/images/stanley/side-image.jpg",
    },
    logoImage: "/images/stanley/logo.png",
    imageList: [
      {
        _id: 1,
        image: "/images/stanley/image-1.jpg",
        caption:
	"1911. gada Stanley Model 63 automobiļu izstādē 1940. gados.",
	source: "Boston Public Library via steamcrazy.com.",
        sourceUrl: "http://steamcrazy.com/",
      },
      {
        _id: 2,
        image: "/images/stanley/image-2.jpg",
        caption:
          "Pols Burdons savā 1912. gada Model 63, ceļā uz 1939. gada Pasaules izstādi Ņujorkā.",
        source:
          "Avots: bruceatkinson.com.",
        sourceUrl: "https://bruceatkinson.com/stanley/donbourdon.shtml",
      },
      {
        _id: 3,
        image: "/images/stanley/image-3.jpg",
        caption:
	"F. E. Stenlijs sēž 1906. gada Stanley Rocket spēkratā, kas novietots pie Stanley remonta nodaļas ēkas. Freds Meriots ar šo automobili uzstādīja pasaules rekordu, sasniedzot 205 km/h lielu ātrumu.",
        source:
          "Avots: Pat Farrell/Stanley Archives via steamcrazy.com.",
        sourceUrl: "http://steamcrazy.com/",
      },
    ],
    imageGallery: {
      caption: "Foto no izstādes “Garāža” atklāšanas 2020. gada 20. februārī.",
      images: [
        {
          _id: 1,
          src: "/images/star/gallery-1.jpg"
        }
      ]
    },
    technicalData: [
      {
        key: "Valsts",
        value: "ASV"
      },
      { 
        key: "Ražošanas gads",
        value: 1911,
      },
      { 
        key: "Motora tilpums",
        value: 3679,
      },
      { 
        key: "Motora jauda",
        value: 10,
      },
      { 
        key: "Cilindru skaits",
        value: "R2",
      },
      { 
        key: "Maksimālais ātrums",
        value: 67,
      },
      { 
        key: "Pašmasa",
        value: 726,
      },
    ]
  },
  {
    _id: "stutz",
    intro: {
      introImage: "/images/stutz/intro-image.jpg",
      introLogoImage: "/images/stutz/intro-logo.png",
      title: <span>STUTZ series A<br />“BearCat”</span>,
      subTitle:
        "Pirmais sērijveidā ražotais sacīkšu auto ASV",
      text: (
        <React.Fragment>
          <p>1911. gadā <a href="https://en.wikipedia.org/wiki/Indianapolis_500" target="_blank">Indianapolis 500</a> sacīkstēs startē Harija Kleitona Staca konstruētais un piecās nedēļās uzbūvētais auto, tas sekmīgi finišē kā vienpadsmitais, distanci nobraucot ar vidējo ātrumu 111 km/h. Pēc šī panākuma H. Stacs dibina uzņēmumu <a href="https://en.wikipedia.org/wiki/Stutz_Motor_Company" target="_blank">Ideal Motor Car Company</a> un uzsāk šī auto ražošanu.</p>
          <p>1912. gadā saražo 266 automobiļus, no tiem 25 spīdsterus.</p>
          <p>Lai arī auto būvēts askētiskā spīdstera izpildījumā (praktiski tā ir ar diviem sēdekļiem, dubļu sargiem un degvielas bāku aprīkota šasija), tas iekaro tirgu un kļūst par kulta auto.</p>
          <p>Automobilim piešķir modeļa nosaukumu “Bearcat” (<a href="https://en.wikipedia.org/wiki/Binturong" target="_blank">Arctictis binturong</a>), tā sauc viveru dzimtas dzīvniekus Dienvidāzijā.</p>
        </React.Fragment>
      ),
      sideImage: "/images/stutz/side-image.jpg",
    },
    logoImage: "/images/stutz/logo.png",
    imageList: [
      {
        _id: 1,
        image: "/images/stutz/image-1.jpg",
        caption:
          "Džons Veins un Morīna O'Hāra 1914. gada Stutz Bearcat automobīlī.",
        source: "Avots: Antique Automobile Club of America Discussion Forums.",
        sourceUrl: "https://forums.aaca.org/topic/251494-john-wayne-and-maureen-ohara-in-a-1914-stutz-bearcat-speedster/",
      },
      {
        _id: 2,
        image: "/images/stutz/image-2.jpg",
        caption:
          "Mēmā kino aktrise Pērla Vaita savā Stutz Bearcat.",
        source:
          "Avots: Antique Automobile Club of America Discussion Forums.",
        sourceUrl: "https://forums.aaca.org/topic/251494-john-wayne-and-maureen-ohara-in-a-1914-stutz-bearcat-speedster/",
      },
      {
        _id: 3,
        image: "/images/stutz/image-3.jpg",
        caption:
          "1915. gada Stutz komanda Indy sacīkstēs. No kreisās puses: Haudijs Vilkokss (ieguva 7. vietu), Ērls Kūpers (4. vieta) un Gils Andersens (3. vieta).",
        source:
          "Avots: Caradisiac Forum-Auto via The Radar.",
        sourceUrl: "http://forum-auto.caradisiac.com/sport-auto/histoire-du-sport-auto/sujet378913.htm",
      },
    ],
    imageGallery: {
      caption: "Foto no izstādes “Garāža” atklāšanas 2020. gada 20. februārī.",
      images: [
        {
          _id: 1,
          src: "/images/stutz/gallery-1.jpg"
        }
      ]
    },
    technicalData: [
      {
        key: "Valsts",
        value: "ASV"
      },
      { 
        key: "Ražošanas gads",
        value: 1912,
      },
      { 
        key: "Motora tilpums",
        value: 6396,
      },
      { 
        key: "Motora jauda",
        value: 50,
      },
      
      { 
        key: "Cilindru skaits",
        value: "R4",
      },
      { 
        key: "Maksimālais ātrums",
        value: 116,
      },
      { 
        key: "Pašmasa",
        value: null,
      },
    ]
  },
  {
    _id: "cadillac",
    intro: {
      introImage: "/images/cadillac/intro-image.jpg",
      introLogoImage: "/images/cadillac/intro-logo.png",
      title: <span>Cadillac Fleetwood<br/>V16 series 37-90</span>,
      subTitle:
        "Kārļa Ulmaņa auto līdzinieks, kas izgatavots tikai 54 eksemplāros",
      text: (
        <React.Fragment>
          <p>Augstas klases septiņvietīgs limuzīns, ražots ASV uzņēmumā <a href="https://en.wikipedia.org/wiki/Cadillac" target="_blank">Cadillac</a>. Automobiļa šasija komplektēta ar tobrīd jaudīgāko Cadillac 16 cilindru motoru un ekskluzīvo virsbūvju ražotāja <a href="https://en.wikipedia.org/wiki/Fleetwood_Metal_Body" target="_blank">Fleetwood</a> izgatavotu virsbūvi. Šīs Cadillac šasijas Fleetwood aprīkoja ar 12 dažāda veida virsbūvēm. Tika izgatavoti tikai 54 automobiļi ar limuzīna virsbūvi, no tiem līdz mūsdienām saglabājušies vien daži.</p>
          <p>Automobiļa salona apdarē izmantoti augstvērtīgi materiāli, vairākas detaļas izgatavotas no koka. Šādu automobili līdz 1940. gadam izmantoja arī Latvijas prezidents Kārlis Ulmanis.</p>
          <p>Automobilis restaurēts un līdz 2019. gadam lietots ASV, kad izsolē to iegādājas kolekcionārs no Latvijas.</p>
        </React.Fragment>
      ),
      sideImage: "/images/cadillac/side-image.jpg",
    },
    logoImage: "/images/cadillac/logo.png",
    imageList: [
      {
        _id: 1,
        image: "/images/cadillac/image-1.jpg",
        caption:
          "John Wayne and Maureen O'Hara in a 1914 Stutz Bearcat speedster.",
        source: "Source: Antique Automobile Club of America Discussion Forums.",
        sourceUrl: "https://forums.aaca.org/topic/251494-john-wayne-and-maureen-ohara-in-a-1914-stutz-bearcat-speedster/",
      },
      {
        _id: 2,
        image: "/images/cadillac/image-2.jpg",
        caption:
          "Valsts un Ministru prezidents Kārlis Ulmanis un ārlietu ministrs Vilhelms Munters pie ieejas “Dauderu” namā. 1937.-1939. gads.",
        source:
          "Avots: Latvijas Nacionālais arhīvs.",
        sourceUrl: "https://www.arhivi.gov.lv/",
      },
      {
        _id: 3,
        image: "/images/cadillac/image-3.jpg",
        caption:
          "The 1915 Stutz team at Indy. Left to right: Howdy Wilcox (finished 7th), Earl Cooper (4th), and Gil Andersen (3rd).",
        source:
          "Source: Caradisiac Forum-Auto via The Radar.",
        sourceUrl: "http://forum-auto.caradisiac.com/sport-auto/histoire-du-sport-auto/sujet378913.htm",
      },
    ],
    imageGallery: {
      caption: "Foto no izstādes “Garāža” atklāšanas 2020. gada 20. februārī.",
      images: [
        {
          _id: 1,
          src: "/images/cadillac/gallery-1.jpg"
        }
      ]
    },
    technicalData: [
      {
        key: "Valsts",
        value: "ASV"
      },
      { 
        key: "Ražošanas gads",
        value: 1937,
      },
      { 
        key: "Motora tilpums",
        value: 7407,
      },
      { 
        key: "Motora jauda",
        value: 185,
      },
      
      { 
        key: "Cilindru skaits",
        value: "V16",
      },
      { 
        key: "Maksimālais ātrums",
        value: null,
      },
      { 
        key: "Pašmasa",
        value: 2800,
      },
    ]
  },
  {
    _id: "audi",
    intro: {
      introImage: "/images/audi/intro-image.jpg",
      introLogoImage: "/images/audi/intro-logo.png",
      title: "Audi 920",
      subTitle:
        "Pēdējais pirmskara Audi modelis – kara trofeja",
      text: (
        <React.Fragment>
          <p>Ražots koncerna <a href="https://en.wikipedia.org/wiki/Auto_Union" target="_blank">Auto Union</a> uzņēmuma <a href="https://en.wikipedia.org/wiki/Horch" target="_blank">Horch</a> rūpnīcā Cvikavā. <em>Audi 920</em> šasija unificēta ar <em>Wanderer W23</em>, taču modelim uzstādīts tobrīd modernais sešcilindru motors ar augšējo sadales vārpstu – <em>Horch</em> astoņcilindru motora konstruktīvs analogs. Kopā saražots 1 281 automobilis, to skaitā 818 kabrioleti, ar <em>Gläser</em> virsbūvju rūpnīcā Drēzdenē izgatavotām virsbūvēm.</p>
          <p>Apskatāmais kabriolets Latvijā nonācis kā kara trofeja un kalpojis līdz 70. gadu beigām. 2017. gadā šo <em>Audi 920</em> atrod izjauktā stāvoklī, taču tam saglabājies gan oriģinālais motors, gan salona ādas tapsējums. Auto iegādājas Motormuzejs restaurācijai.</p>
        </React.Fragment>
      ),
      sideImage: "/images/audi/side-image.jpg",
    },
    logoImage: "/images/audi/logo.png",
    scanGallery: {
      caption: "Audi 920 1940. gada brošūra",
      images: [
        {
          _id: 1,
          src: "/images/audi/scan-1.jpg"
        }
      ]
    },
    imageList: [
      {
        _id: 1,
        image: "/images/audi/image-1.jpg",
        caption:
          "John Wayne and Maureen O'Hara in a 1914 Stutz Bearcat speedster.",
        source: "Source: Antique Automobile Club of America Discussion Forums.",
        sourceUrl: "https://forums.aaca.org/topic/251494-john-wayne-and-maureen-ohara-in-a-1914-stutz-bearcat-speedster/",
      },
      {
        _id: 2,
        image: "/images/audi/image-2.jpg",
        caption:
          "Valsts un Ministru prezidents Kārlis Ulmanis un ārlietu ministrs Vilhelms Munters pie ieejas “Dauderu” namā. 1937.-1939. gads.",
        source:
          "Avots: Latvijas Nacionālais arhīvs.",
        sourceUrl: "https://www.arhivi.gov.lv/",
      },
      {
        _id: 3,
        image: "/images/audi/image-3.jpg",
        caption:
          "The 1915 Stutz team at Indy. Left to right: Howdy Wilcox (finished 7th), Earl Cooper (4th), and Gil Andersen (3rd).",
        source:
          "Source: Caradisiac Forum-Auto via The Radar.",
        sourceUrl: "http://forum-auto.caradisiac.com/sport-auto/histoire-du-sport-auto/sujet378913.htm",
      },
    ],
    imageGallery: {
      caption: "Foto no izstādes “Garāža” atklāšanas 2020. gada 20. februārī.",
      images: [
        {
          _id: 1,
          src: "/images/audi/gallery-1.jpg"
        }
      ]
    },
    technicalData: [
      {
        key: "Valsts",
        value: "Vācija"
      },
      { 
        key: "Ražošanas gads",
        value: 1940,
      },
      { 
        key: "Motora tilpums",
        value: 3281,
      },
      { 
        key: "Motora jauda",
        value: 75,
      },
      
      { 
        key: "Cilindru skaits",
        value: "R6",
      },
      { 
        key: "Maksimālais ātrums",
        value: 130,
      },
      { 
        key: "Pašmasa",
        value: 1670,
      },
    ]
  },
  {
    _id: "renault",
    intro: {
      introImage: "/images/renault/intro-image.jpg",
      introLogoImage: "/images/renault/intro-logo.png",
      title: <span>Renault<br/>Nervasport</span>,
      subTitle:
        "Viens no diviem kabrioletiem, kas saglabājušies līdz mūsdienām",
      text: (
        <React.Fragment>
          <p>Sporta automobilis, ražots Francijas uzņēmumā <a href="https://en.wikipedia.org/wiki/Renault" target="_blank">Renault</a>. <em>Nervasport</em> modeļu koncepcija – jaudīgs motors uz vieglas šasijas. ACN1 ir jaudīgākais <em>Nervasport</em> modelis. Virsbūve izgatavota uzņēmumā Stella. Modeli ACN1 ražo tikai pusgadu – no 1935. gada marta līdz septembrim, izgatavojot dažus desmitus eksemplāru, tostarp astoņus kabrioletus, no kuriem līdz mūsdienām saglabājušies tikai divi.</p>
          <p>Apskatāmais automobilis kopš 1935. gada lietots Somijā. Vispirms Francijas vēstniecībā, pēc tam divreiz mainījis īpašniekus. Līdz 2019. gadam, kad to iegādājas kolekcionārs no Latvijas, auto eksponēts <em>Vehoniemen</em> automuzejā Somijā.</p>
        </React.Fragment>
      ),
      sideImage: "/images/renault/side-image.jpg",
    },
    logoImage: "/images/renault/logo.png",
    imageList: [
      {
        _id: 1,
        image: "/images/renault/image-1.jpg",
        caption:
          "Izstādē redzamais Renault Nervasport 1970. gados.",
        source: "",
        sourceUrl: "",
      },
      {
        _id: 2,
        image: "/images/renault/image-2.jpg",
        caption:
          "Valsts un Ministru prezidents Kārlis Ulmanis un ārlietu ministrs Vilhelms Munters pie ieejas “Dauderu” namā. 1937.-1939. gads.",
        source:
          "Avots: Latvijas Nacionālais arhīvs.",
        sourceUrl: "https://www.arhivi.gov.lv/",
      },
      {
        _id: 3,
        image: "/images/renault/image-3.jpg",
        caption:
          "The 1915 Stutz team at Indy. Left to right: Howdy Wilcox (finished 7th), Earl Cooper (4th), and Gil Andersen (3rd).",
        source:
          "Source: Caradisiac Forum-Auto via The Radar.",
        sourceUrl: "http://forum-auto.caradisiac.com/sport-auto/histoire-du-sport-auto/sujet378913.htm",
      },
      {
        _id: 4,
        image: "/images/renault/image-4.jpg",
        caption:
          "The 1915 Stutz team at Indy. Left to right: Howdy Wilcox (finished 7th), Earl Cooper (4th), and Gil Andersen (3rd).",
        source:
          "Source: Caradisiac Forum-Auto via The Radar.",
        sourceUrl: "http://forum-auto.caradisiac.com/sport-auto/histoire-du-sport-auto/sujet378913.htm",
      },
    ],
    imageGallery: {
      caption: "Foto no izstādes “Garāža” atklāšanas 2020. gada 20. februārī.",
      images: [
        {
          _id: 1,
          src: "/images/renault/gallery-1.jpg"
        }
      ]
    },
    technicalData: [
      {
        key: "Valsts",
        value: "Francija"
      },
      { 
        key: "Ražošanas gads",
        value: 1935,
      },
      { 
        key: "Motora tilpums",
        value: 5447,
      },
      { 
        key: "Motora jauda",
        value: 125,
      },
      
      { 
        key: "Cilindru skaits",
        value: "R8",
      },
      { 
        key: "Maksimālais ātrums",
        value: 125,
      },
      { 
        key: "Pašmasa",
        value: 1800,
      },
    ]
  },
  {
    _id: "rolls-royce",
    intro: {
      introImage: "/images/rolls-royce/intro-image.jpg",
      introLogoImage: "/images/rolls-royce/intro-logo.png",
      title: <span>Rolls-Royce<br/>Silver Spirit “Kroko”</span>,
      subTitle:
        "Krokodila ādas salons un apzeltīti riteņu spieķi šo Rolls Royce padara unikālu",
      text: (
        <React.Fragment>
          <p>Aplūkojamais kabriolets ir pēc individuāla pasūtījuma itāļu uzņēmumā <em>Autoconstruzioni Torino</em> pārbūvēts <a href="https://en.wikipedia.org/wiki/Rolls-Royce_Motors" target="_blank">Rolls-Royce</a> sedans. Tā salons ir tapsēts ar krokodila ādu, tādēļ to dēvē par “Kroko”. Automobilis aprīkots ar televizoru, videomagneto-fonu un divām atpakaļskata kamerām ar monitoru. Uzstādīta tā laika vispilnākās komplektācijas Clarion audio sistēma. Individuāli izgatavoto riteņu spieķi ir apzeltīti, apzeltīta ir arī dievietes statuja, kas uzstādīta uz radiatora.</p>
          <p>Izgatavoti tikai trīs automobiļi ar šādu aprīkojumu. To pārbūves izmaksas samērojamas ar vienģimenes mājas cenu ASV.</p>
          <p>2019. gadā automobili iegādājies kolekcionārs no Latvijas.</p>
        </React.Fragment>
      ),
      sideImage: "/images/rolls-royce/side-image.jpg",
    },
    logoImage: "/images/rolls-royce/logo.png",
    imageList: [
      {
        _id: 1,
        image: "/images/rolls-royce/image-1.jpg",
        caption:
          "Standarta pirmās paaudzes Rolls Royce Silver Spirit sedans.",
        source: "Avots: Rolls Royce Silver Spirit via WikiCommons.",
        sourceUrl: "https://commons.wikimedia.org/wiki/File:1985_Rolls_Royce_Silver_Spirit_(21447864283).jpg",
      },
      {
        _id: 2,
        image: "/images/rolls-royce/image-2.jpg",
        caption:
          "Standarta pirmās paaudzes Rolls Royce Silver Spirit sedans.",
        source: "Avots: Rolls Royce Silver Spirit via WikiCommons.",
        sourceUrl: "https://commons.wikimedia.org/wiki/File:1985_Rolls_Royce_Silver_Spirit_(22056542592).jpg",
      },
      {
        _id: 3,
        image: "/images/rolls-royce/image-3.jpg",
        caption:
          "Izstādē redzamais auto ir viens no trīs izdevēju brāļu Fabri pasūtītajiem Rolls Royce Silver Spirit kabrioletiem. Attēlā: brāļi Fabri 1950. gados (no kreisās: Dino, Džovanni un Rino).",
        source: "Avots: Il Giornale.",
        sourceUrl: "https://www.stefanolorenzetto.it/pagine/interviste/Fabbri.pdf",
      },
    ],
    imageGallery: {
      caption: "Foto no izstādes “Garāža” atklāšanas 2020. gada 20. februārī.",
      images: [
        {
          _id: 1,
          src: "/images/rolls-royce/gallery-1.jpg"
        }
      ]
    },
    technicalData: [
      {
        key: "Valsts",
        value: "Lielbritānija"
      },
      { 
        key: "Ražošanas gads",
        value: 1987,
      },
      { 
        key: "Motora tilpums",
        value: 6750,
      },
      { 
        key: "Motora jauda",
        value: 240,
      },
      
      { 
        key: "Cilindru skaits",
        value: "V8",
      },
      { 
        key: "Maksimālais ātrums",
        value: 195,
      },
      { 
        key: "Pašmasa",
        value: null,
      },
    ]
  },
];

export function getContent() {
  return content;
}

export function getContentPart(id) {
  return content.find((x) => x._id === id);
}
