import React from "react";
import Intro from "../components/Intro/Intro";
import ImageList from "../components/ImageList/ImageList";
import Gallery from "../components/Gallery/Gallery";
import CarData from "../components/CarData/CarData";

const Article = (props) => {
  return (
    <article className="Story" id={props.id}>
      <Intro id={props.id} content={props.content.intro} />
      {props.id === 'article-audi' ? (
        <Gallery content={props.content.scanGallery} />
      ) : null}
      <ImageList content={props.content.imageList} />
      <Gallery content={props.content.imageGallery} />
      <CarData
        content={props.content.technicalData}
        logo={props.content.logoImage}
      />
    </article>
  );
};

export default Article;
