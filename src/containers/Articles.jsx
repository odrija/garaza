import React, { Component } from "react";
import HorizontalScroll from 'react-scroll-horizontal';
import { getContent } from "../services/contentService";
import Article from "./Article";

class Articles extends Component {
  state = {
    content: getContent(),
  };

  calculateOffset(id) {
    const content = this.state.content;
    const currentEl = content.find((el) => el._id === id);
    const index = content.indexOf(currentEl);
    if (index !== 0) {
      const prevEl = document.getElementById(
        `article-${content[index - 1]._id}`
      );
      let totalWidth = 0;

      for (let i = 0; i < prevEl.children.length; i++) {
        totalWidth += prevEl.children[i].offsetWidth;
      }

      const offsetWidth = totalWidth - prevEl.offsetWidth;
      document.getElementById(
        `article-${id}`
      ).style.transform = `translateX(${offsetWidth}px)`;
    }
  }
  
  calculateArticleWidth(id) {
    const article = document.getElementById(
      `article-${id}`
    );
    let totalWidth = 0;
    for (let i = 0; i < article.children.length; i++) {
      totalWidth += article.children[i].offsetWidth;
    }
    article.style.width = `${totalWidth}px`;
  }

  // setArticleOffsets() {
  //   this.state.content.map((article) => {
  //     this.calculateOffset(article._id);
  //   });
  // }
  
  setArticleWidths() {
    this.state.content.map((article) => {
      this.calculateArticleWidth(article._id);
    });
  }

  componentDidMount() {
    window.addEventListener("resize", () => this.setArticleWidths());
    window.addEventListener("load", () => this.setArticleWidths());
  }

  render() {
    return (
      <HorizontalScroll>
        {this.state.content.map((article) => (
          <Article
            id={`article-${article._id}`}
            key={article._id}
            content={article}
          />
        ))}
      </HorizontalScroll>
    );
  }
}

export default Articles;
