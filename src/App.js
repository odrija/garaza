import React from "react";
import Articles from "./containers/Articles";
import "./App.scss";

function App() {
  return <Articles />;
}

export default App;
