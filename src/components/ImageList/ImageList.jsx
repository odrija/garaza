import React from "react";
import "./ImageList.scss";

const ImageList = (props) => {
  const { content } = props;

  return (
    <div className="ImageList">
      <div className="ImageList__container">
        {content.map((figure) => (
          <figure key={figure._id} className="ImageList__item">
            <img src={figure.image} />
            <figcaption>
              {figure.caption}{" "}
              <a
                href={figure.sourceUrl}
                className="ImageList__source"
                target="_blank"
              >
                {figure.source}
              </a>
            </figcaption>
          </figure>
        ))}
      </div>
    </div>
  );
};

export default ImageList;
