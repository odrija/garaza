import React from "react";
import "./CarData.scss";

const CarData = (props) => {
  const { content, logo } = props;

  return (
    <div className="CarData">
      <div className="CarData__logo">
        <img src={logo} />
      </div>
      <div className="CarData__table">
        <h3>Tehniskie dati</h3>
        {content.map((row, i) => {
          if (row.value) {
            let displayValue = '';
            switch (row.key) {
              case 'Motora tilpums':
                displayValue = <>{`${row.value} cm`}<sup>3</sup></>;
                break;
                
              case 'Motora jauda':
                displayValue = `${row.value} HP`;
                break;
                
              case 'Maksimālais ātrums':
                displayValue = `${row.value} km/h`;
                break;
                
              case 'Pašmasa':
                displayValue = `${row.value} kg`;
                break;
            
              default:
                displayValue = row.value;
                break;
            }
            return (
              <div className="CarData__table__row" key={i}>
                <span>{row.key}</span>
                <span>{displayValue}</span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
};

export default CarData;
