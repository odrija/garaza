import React from "react";
import "./Gallery.scss";

const Gallery = (props) => {
  const { content } = props;

  return (
    <div className="Gallery">
      <div className="Gallery__container">
        <button className="Gallery__arrow Gallery__arrow--left"></button>
        <button className="Gallery__arrow Gallery__arrow--right"></button>
        <div className="Gallery__items">
          {content.images.map((img) => (
            <div className="Gallery__item" key={img._id}>
              <img src={img.src} />
            </div>
          ))}
        </div>
        <div className="Gallery__caption">
          {content.caption}
          <span className="Gallery__counter">2/6</span>
        </div>
      </div>
    </div>
  );
};

export default Gallery;
