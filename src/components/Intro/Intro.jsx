import React, { Component } from "react";
import "./Intro.scss";

class Intro extends Component {
  constructor(props) {
    super(props);
    this.calculateHeadingSize = this.calculateHeadingSize.bind(this);
  }
  
  calculateHeadingSize() {
    const { id } = this.props;
    const title = document.getElementById(`${id}-title`);
    const windowHeight = window.innerHeight;
    const windowWidth = window.innerWidth;
    
    let fontSize = false;
    
    if (windowHeight <= 800 && windowWidth <= 1400) {
      switch (id) {
        case 'article-star':
          fontSize = '8rem';
          break;
          
        case 'article-stutz':
        case 'article-stanley':
          fontSize = '5.5rem';      
          break;
      
        case 'article-cadillac':
          fontSize = '5rem';
          break;

        default:
          break;
      }
    } else if (windowHeight > 800 && windowHeight <= 1024 && windowWidth <= 1400) {
      switch (id) {
        case 'article-cadillac':
          fontSize = '8rem';
          break;

        default:
          break;
      }
    } else if (windowHeight >= 800 && windowWidth > 1400) {
      switch (id) {
        case 'article-stutz':
        case 'article-renault':
          fontSize = '6.8rem';
          break;

        case 'article-cadillac':
        case 'article-stanley':
        case 'article-rolls-royce':
          fontSize = '6rem';
          break;

        default:
          break;
      }
    } else if (windowHeight >= 1080 && windowWidth >= 1400) {
      switch (id) {
        case 'article-star':
          fontSize = '10rem';
          break;

        case 'article-stutz':
        case 'article-cadillac':
          fontSize = '8rem';
          break;

        case 'article-stanley':
        case 'article-renault':
          fontSize = '6.8rem';
          break;

        case 'article-rolls-royce':
          fontSize = '6rem';
          break;

        default:
          break;
      }
    }
    
    if (fontSize) {
      title.style.fontSize = fontSize;
    }
  }
  
  componentDidMount() {
    window.addEventListener("load", () => this.calculateHeadingSize());
    window.addEventListener("resize", () => this.calculateHeadingSize());
  }

  render() { 
    const { id, content } = this.props;
    const introStyles = {
      backgroundImage: id === 'article-stanley' ? null : `url(${content.introLogoImage})`,
      backgroundSize: id === 'article-audi' || id === 'article-rolls-royce' ? 'auto 100%' : null,
      paddingRight: id === 'article-cadillac' ? '13.6rem' : id === 'article-rolls-royce' ? '10rem' : null,
    };

    return (
      <React.Fragment>
        <div className="Intro__image">
          <img src={content.introImage} alt={content.title} />
        </div>
        <div className="Intro__content" style={introStyles}>
          <h1 id={`${id}-title`} className="Intro__title">
            {content.title}
          </h1>
          {id === "article-stanley" ? (
            <div className="Intro__columns">
              <div className="Intro__column">
                <h2 className="Intro__subtitle">{content.subTitle}</h2>
                <div className="Intro__body">{content.text}</div>
              </div>
              <div className="Intro__column Intro__column--images">
                <img
                  className="Intro__column__image"
                  src="/images/stanley/intro-1.jpg"
                />
                <img
                  className="Intro__column__image"
                  src="/images/stanley/intro-2.jpg"
                />
              </div>
            </div>
          ) : (
            <div>
              <h2
                className="Intro__subtitle"
                style={id === 'article-renault' ? { width: '51%' } : null}
              >{content.subTitle}</h2>
              <div className="Intro__body">{content.text}</div>
            </div>
          )}
        </div>
        <img
          className="Intro__sideImage"
          src={content.sideImage}
          alt={content.title}
        />
        {id === "article-stanley" ? (
          <div
            className="Intro__content"
            style={{
              backgroundImage: `url(${content.introLogoImage})`,
              backgroundPosition: "left bottom",
              backgroundColor: "rgba(233, 223, 206, .2)",
              paddingRight: "10rem",
            }}
          >
            <div
              className="Intro__body Intro__columns"
              style={{ maxWidth: "115rem" }}
            >
              <div className="Intro__column">{content.text2}</div>
              <div className="Intro__column Intro__column--images">
                <img
                  className="Intro__column__image"
                  src="/images/stanley/intro-3.jpg"
                />
                <img
                  className="Intro__column__image"
                  src="/images/stanley/intro-4.jpg"
                />
              </div>
            </div>
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}

export default Intro;
